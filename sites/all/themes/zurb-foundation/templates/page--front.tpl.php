<!--.page -->
<div role="document" class="page">

  <!--.l-header -->
  <header role="banner" class="l-header">

    <?php if ($top_bar): ?>
      <!--.top-bar -->
      <?php if ($top_bar_classes): ?>
        <div class="<?php print $top_bar_classes; ?>">
      <?php endif; ?>
      <nav class="top-bar"<?php print $top_bar_options; ?>>
        <ul class="title-area">          
          <li class="toggle-topbar menu-icon"><a href="#"><span><?php print $top_bar_menu_text; ?></span></a></li>
        <?php if ($logo): ?>
        <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" id="logo"/>
        </a>
      <?php endif; ?>
        </ul>
        <section class="top-bar-section">
          <?php if ($top_bar_secondary_menu) :?>
            <?php print $top_bar_secondary_menu; ?>
          <?php endif; ?>
          <?php if ($top_bar_main_menu) :?>
            <?php print $top_bar_main_menu; ?>
          <?php endif; ?>          
        </section>
      </nav>
      <?php if ($top_bar_classes): ?>
        </div>
      <?php endif; ?>
      <!--/.top-bar -->
    <?php endif; ?>

    <!-- Title, slogan and menu -->
    <?php if ($alt_header): ?>
      <section class="row <?php print $alt_header_classes; ?>">

        <?php if ($linked_logo): print $linked_logo; endif; ?>

        <?php if ($site_name): ?>
          <?php if ($title): ?>
            <div id="site-name" class="element-invisible">
              <strong>
                <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
              </strong>
            </div>
          <?php else: /* Use h1 when the content title is empty */ ?>
            <h1 id="site-name">
              <a href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>" rel="home"><span><?php print $site_name; ?></span></a>
            </h1>
          <?php endif; ?>
        <?php endif; ?>

        <?php if ($site_slogan): ?>
          <h2 title="<?php print $site_slogan; ?>" class="site-slogan"><?php print $site_slogan; ?></h2>
        <?php endif; ?>

        <?php if ($alt_secondary_menu): ?>
          <nav id="secondary-menu" class="navigation" role="navigation">
            <?php print $alt_secondary_menu; ?>
          </nav> <!-- /#secondary-menu -->
        <?php endif; ?>

        <?php if ($alt_main_menu): ?>
          <nav id="main-menu" class="navigation" role="navigation">
            <?php print ($alt_main_menu); ?>
          </nav> <!-- /#main-menu -->
        <?php endif; ?>

         </section>
    <?php endif; ?>
    <!-- End title, slogan and menu -->
        <?php if (!empty($page['header'])): ?>
      <!--.l-header-region -->
      <section class="l-header-region row">
        <div class="large-12 columns">
          <?php print render($page['header']); ?>          
        </div>
      </section>      
      <!--/.l-header-region -->      
    <?php endif; ?>    
  </header>
    <!--/.l-header -->

  <?php if (!empty($page['featured'])): ?>
    <!--.featured -->
    <section class="l-featured row">
      <div class="large-12 columns">
        <?php print render($page['featured']); ?>
      </div>
    </section>
    <!--/.l-featured -->
  <?php endif; ?>

  <?php if ($messages && !$zurb_foundation_messages_modal): ?>
    <!--.l-messages -->
    <section class="l-messages row">
      <div class="large-12 columns">
        <?php if ($messages): print $messages; endif; ?>
      </div>
    </section>
    <!--/.l-messages -->
  <?php endif; ?>

  <?php if (!empty($page['help'])): ?>
    <!--.l-help -->
    <section class="l-help row">
      <div class="large-12 columns">
        <?php print render($page['help']); ?>
      </div>
    </section>
    <!--/.l-help -->
  <?php endif; ?>
           <div class="panel-pane pane-block pane-boxes-homepage-description block-boxes-simple ">
      <div id="boxes-box-homepage_description" class="boxes-box"><div class="boxes-box-content"> <div class="hero-unit"><h1>BE MODERN<br>USE A FLAT STYLING<br>WE ARE WAITING FOR YOU, WORK WITH US<br>AND HAVE FUN LIKE WE</h1>
      <div class="center-buttom"><a href="#" class="secondary small button2">JOIN US TODAY</a></div>
      </div>
      </div></div>
    </div>
      <div class="row">
    <div class="large-4 columns">      
      <img class="large4img"  src="/sites/all/themes/zurb-foundation/images/cus.png"><h4>CUSTOMIZE</h4>      
<p>Crucifix aesthetic vinyl pork belly.Meh +1 typewrite, Williamsburg irony viral single-origin coffee</p>
    </div>
    
    <div class="large-4 columns">     
      <img class="large4img"  src="/sites/all/themes/zurb-foundation/images/res.png"><h4>RESPONSIVE</h4>      
<p>Crucifix aesthetic vinyl pork belly.Meh +1 typewrite, Williamsburg irony viral single-origin coffee</p>
    </div>
    
    <div class="large-4 columns">
      <img class="large4img"  src="/sites/all/themes/zurb-foundation/images/mo.png"><h4>MONEY</h4>
      <p>Crucifix aesthetic vinyl pork belly.Meh +1 typewrite, Williamsburg irony viral single-origin coffee</p>
    </div>
      </div>
      <hr>
       <div class="hero-uni">
      
          <img class="pc" src="/sites/all/themes/zurb-foundation/images/pc.png">

        <div class="large-5 columns letter">
          <h4 class="letter">This is a content section.</h4>
          <p>Bacon ipsum dolor sit amet nulla ham qui sint exercitation eiusmod commodo, chuck duis velit. Aute in reprehenderit, dolore aliqua non est magna in labore pig pork biltong. Eiusmod swine spare ribs reprehenderit culpa. Boudin aliqua adipisicing rump corned beef.</p>
          <p><a href="#" class="secondary small button">HIRE US</a></p>
      </div>
       </div>
         <div class="row">     
         <h4 class="letter2">CHECK OUT</h4>
         <h5 class="letter2">OUR WORKS NOW</h5>             
    <div class="large-4 columns">
      <img class="pc" src="/sites/all/themes/zurb-foundation/images/image1.png"/>
      <h4>This is a content section.</h4>
      <p>Bacon ipsum dolor sitmp corned beef.</p>
    </div>
    
    <div class="large-4 columns">
      <img class="pc" src="/sites/all/themes/zurb-foundation/images/image2.png"/>
      <h4>This is a content section.</h4>
      <p>Bacon ipsum dolor sitmp corned beef.</p>
    </div>
    
    <div class="large-4 columns">
      <img class="pc" src="/sites/all/themes/zurb-foundation/images/image3.png"/>
      <h4>This is a content section.</h4>
      <p>Bacon ipsum dolor sitrned beef.</p>
     <p><a href="#" class="secondary small button">VIEW MORE</a></p>
    </div>
    </div>
     <div class="sliderfront">
     <h3>WHAT OUR</h3> 
     <h5>CLIENTS SAYS ABOUT US</h5>
     <h6><comi>"</comi>Lorem Ipsum. Proin gravida nibh vel velit auctor aliquet. Aenean sollicitudin, lorem quis <br> biberndum auctor, nisi elit consequat ipsum, nec sagittis sem nibh id elit. Duis sed odio sit amet <br> nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit. Nam nec tellus a odio <br> tincidunt auctor a ornare odio.<comi>"</comi> - <jo>Johnny Strawberry</jo></h6>           
     </div>
     <div class="comment">
     <div class="rew">
        <div class="large-4 columns">
      <h3>This is a content section.</h3>
      <h4>ccxcxcxisicing rump corned beef.</h4>
      <p><a href="#" class="secondary small button">VIEW MORE</a></p>
    </div>
        <div class="large-4 columns">
      <img  src="/sites/all/themes/zurb-foundation/images/flo.png"/>
      <h4>This is a content section.</h4>
    </div>    
        <div class="large-4 columns">
      <img src="/sites/all/themes/zurb-foundation/images/mon.png"/>
      <h4>This is a content section.</h4>
    </div>
    </div>
    </div>
      <div class="footerrr">
      <div class="raw">      
        <div class="medium-4 large-4 columns">
          <h3>Title of Content</h3>
          <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus illo debitis, enim qui nemo harum perspiciatis inventore, facere omnis neque ipsam.</p>
        </div>       
         <div class="medium-4 large-4 col">
          <h3>Title of Content</h3>
          <li><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Voluptatibus illo debitis, enim qui nemo harum perspiciatis inventore, facere omnis neque ipsam.</p></li>
        </div>            
    <div class="large-4 columns"> 
         <h3>Title of Content</h3>
      <ul class="small-block-grid-3">
        <li><a href="#"><img src="/sites/all/themes/zurb-foundation/images/logos de flickr1.png"/></a></li>
        <li><a href="#"><img src="/sites/all/themes/zurb-foundation/images/logos flickr 2.png"/></a></li>
        <li><a href="#"><img src="/sites/all/themes/zurb-foundation/images/logos flickr 3.png"/></a></li>        
      </ul>
    </div>
    </div>            
  </div>       
      </div>

    <div class="footer2">
    <?php if ($site_name) :?>
      <div class="finish">
        <div class="copyright large-12 columns">
          &copy; <?php print date('Y') . ' ' . t('CREATED BY') . ' ' . check_plain($site_name); ?>
        </div>
        </div>
      <?php endif; ?>
    </div>
     <!--/.l-main region -->

    <?php if (!empty($page['sidebar_first'])): ?>
      <aside role="complementary" class="<?php print $sidebar_first_grid; ?> l-sidebar-first columns sidebar">
        <?php print render($page['sidebar_first']); ?>
      </aside>
    <?php endif; ?>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside role="complementary" class="<?php print $sidebar_sec_grid; ?> l-sidebar-second columns sidebar">
        <?php print render($page['sidebar_second']); ?>
      </aside>
    <?php endif; ?>
  </main>
  <!--/.l-main-->

  <?php if (!empty($page['footer_first']) || !empty($page['footer_middle']) || !empty($page['footer_last'])): ?>
    <!--.l-footer-->
    <footer class="l-footer panel row" role="contentinfo">
      <?php if (!empty($page['footer_first'])): ?>
        <div id="footer-first" class="large-4 columns">
          <?php print render($page['footer_first']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($page['footer_middle'])): ?>
        <div id="footer-middle" class="large-4 columns">
          <?php print render($page['footer_middle']); ?>
        </div>
      <?php endif; ?>
      <?php if (!empty($page['footer_last'])): ?>
        <div id="footer-last" class="large-4 columns">
          <?php print render($page['footer_last']); ?>
        </div>
      <?php endif; ?>

      <?php if ($site_name) :?>
        <div class="copyright large-12 columns">
          &copy; <?php print date('Y') . ' ' . check_plain($site_name) . ' ' . t('All rights reserved.'); ?>
        </div>
      <?php endif; ?>
    </footer>
    <!--/.footer-->
  <?php endif; ?>

  <?php if ($messages && $zurb_foundation_messages_modal): print $messages; endif; ?>
</div>
<!--/.page -->
